# 2021-09-23 #

### Feedback about Conflict View
* In my opinion, I am sometimes in harmony view, and sometimes in conflict view. 
* It all depends on the confidence of how much did I know about the issue. 
* If I can handle the situation, I will be a positive person when facing conflicts. 
* Something like academic issues or something that I can make my own decisions, I will be fine. 
* However, there are still some problems that destroyed my mind. 
* For example, my girl friend got epilepsy last two weeks. That was the most serious conflict I’ve never met before. 
* When something out of control, It seems that I would change to a person in harmony view.

### Difference between Group and Team
* Group: Only be grouped together, but don’t know each other.
* Team: Work together smoothly and achieve same goals.

# 2021-09-30 #

### Feedback about "We are in The Era of Code"
* Professor told us that in nowadays world, we are strongly connected with Codes.
* In my opinion, I thought that this comment is correct.
* Not only in the domain of Computer Science, but also in the domain of Mechanical Engineering,we all need to learn the computer language! 
* this will be the world,s trend.

### Feedback of Steven Pinker's Claim
* I would like to share my own opinion about his claim.
* Obviously, in his presentation, he had told us that we are in the era that progresses in a good way now.
* However, there are still lots of issues that he hadn't discuss in his report.
* He had just talked about changes in "Human", but there a large amount of species that he hadn't discussed.
* On the other side of those pros to human, animals and plants are suffered from dangerous situations.
* We should make the conclusion in more cautious way.