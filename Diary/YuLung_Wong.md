﻿Diary written by YuLung_Wong
Student ID E14052021

# 2018/09/27 
* Presentation

    1. I need to put on when do I visit the data last time in the reference.
	2. Still not so clear about public domain.
	3. In my presentation,I claim that more people died because of war in 1940,but that accually not an objective fact because the WWII.
	
* First video ：*3 ways to spot a bad statistic | Mona Chalabi*
  
    1. When you hear a statistic,you might feel skeptical. As soon as it's buried in a chart,it feels like some kind of objective science,and it's not.
	2. Don't be confused by average,sometimes it may ignored some important fact.
	3. Private companies don't have a huge interest in getting the numbers right,they just need the right numbers.
	
* Second video：*The best stats you've ever seen | Hans Rosling*

    1. There accually have huge diffirence between afica contries,but we always think they are just the same.
	2. When I was watching this video,I realized I accually was confused by average.
	3. Some average numbers looks simple,but it may contain huge diffirence inside,we need to see deeper.
	
# 2018/10/4
* First video： *Prof. Werner brilliantly explains how the banking system and financial sector really work.*

    1. The word "deposit" is meaningless in law.
	2. The UK doesn't have finance, the city of London has and is not part of the UK.
	
	
* Second video： *How does the economy really work?*
    1. I think the short term debt cycle is pretty interesting one.
	2. This video blew my mind, refreshing the economic concept I had before.
	3. The long term debt cycle is very scary, but I thought maybe we are at the bottom of the curve.
	
# 2018-10-11
* Presentation
    1. You should write your claim on the title, no question mark on your title.
	
* First video：*Why fascism is so tempting — and how your data could power it*
    1. Countries with strong sence of nationalism are more prosperous and peaceful.
	2. learn about fascism.
	
*  Teacher's claim
    1. The value of the cash is base on the believe.
	2. The important property of money is that it must storage value.
	
#2018-10-18
* Presentiaion
    1. Need to present whole data.
	
* First video： *The brain-changing benefits of exercise*
    1. I think i should start to do some exercise, maybe jogging everyday.
	2. I seldom do exercise now, I feel tired easily everyday, and this would make me hard to study.
	
* Second video： *What if we paid doctors to keep people healthy?*
    1. Doctor should keep people healthy when they were not sick.
	2. A study by a US national newspaper estimated that in the United States, up to 30 percent of all surgical procedures, including stent and pacemaker implantations, hip replacements and uterus removals, were conducted, although other nonsurgical treatment
        options had not been fully exploited
		
#2018-10-25
* First video： *how you can connect to friends who are depressed*
   1. i have no experience about communicating with depressed people, but i agreed what he said in video, we should treat them like a normal but not a depression people and listen what them want to say.
   
* Second video：*The bridge between suicide and life*
  1. I always believe the situation will get better when i was depressed, and this thought give me a lot of power to keep going.
  
#2018-11-1
* First video：*Do you really know why you do what you do?*
  1. "it is actually very hard to ever prove that people are wrong about themselves." I have strong feeling about this, and some people may force you to agree with their perspective, very annoy.
  2. The experiment in the video is really interesting, people tend to explain their choice after they made the choice.
* Second video：*What comes after tragedy? Forgiveness*
  1. I think violence is a learned behavior.
  2. no violence also can be taught.
  
#2018-11-8
  * First video：
    1. I didn't know it's I don't need to show my personal data to the police if I am not commit crime, I thought it's necessary if police ask me to provide personal data.
    
#2018-11-15
  * How we need to remake the internet：
    1. It's really shocking that ads on the internet are modifying our behavior, it's really scary.
    2. I agree with him, I would glad to paid if I can get better service on the internet.
 * Simon Sinek on Millennials in the Workplace：
    1. engagement with social media and our cellphones releass a chemical called dopamines.
    2. social media is highly addictive.
    3. We need to learn is patience.
    
#2018-11-22
   * Fall Of Empires: Rome vs USA：
     1. They clipped the edge off of their gold or silver coin, and they save up all of those coin clipping melt them down, and made more coin. That's really interesting.
     2. They forced people to do the job on the book, if you don't, and you will be dead. That's crazy!
     
#2018-11-29
  * A healthy economy should be designed to thrive, not grow (zh)
    1.  "And then, the question beyond, where history offers us only fragments, what to do when the increase in real income itself lose its charm?"  -- W.W.Rostow
    2.  Keep progression all the time maybe is not a good thing.
    
#2018-12-6
  * The third industrial revolution: a radical new sharing economy
    1. Zero marginal cost is a huge point about the third industrial revolution, the future blueprint mentioned in the video is very facinating.
    2. And what video said is already happening right now.
    3. If our businesses are still plugged in to a 2nd Industrial Revolution infrastructure, we can't get above the ceiling of 20% aggregate efficiency.
    
#2018-12-13
   * Analyse how you feel successful and produtive.
     1.  If I finish my job which I plan to do with out wasting too much time, than I'll feel successful and produtive. Sometimes I just space out a lot of time, though I'll finish the job eventually
          , but I'll feel bad wasting too much time and doing nothing. To be more produtive, I need to remind myself don't waste time doing nothing.


  * five rules
     1. If I feel tired, and keep drifting off, than I should have some rest.
     2. Don't study after 11pm.
     3. Concentrate on what I am doing.
     4. Determine direction before start.
     5. Separate a big thing into small things.
#Produtive and Successful
   * 2018-12-21
     1. I went to Taipei and help my brother for Taipei International Audio&Art Show.
     2. I feel productive, because I learned a lot about audio system engineering.
     3. I feel successful, because I help my brother a lot, and I was busy whole day.
   * 2018-12-22
     1. I went date with my girlfriend on Saturday.
     2. Productive, because I played around with my girlfriend whole day, I think no time was wasted, it was a good time.
     3. Successful, because I had a lot of fun.
   * 2018-12-23
     1. It was the last day of the exhibition, so I helped my brother all the day.
     2. I feel productive, because I learned a lot.
     3. I feed successful, because I can explain more detail about the product to the visitor.
   * 2018-12-24
     1. After finished quiz and expriment in the morning, I went home and played computer game.
     2. I feel little bit unproductive, because I think there was an hour I just doing nothing, not even playing game.
     3. I feel successful, it still was a great relax for me after the midterm and the exhibition.
     4. To be more productive, I should remind myself I was doing nothing when I drifted out.
  * 2018-12-25
     1. After the class, I had my lunch and went back to bed, slept for an hour, then I finished the task which my brother gave me,then I played computer game.
     2. At night, I had a discussion about the project of automatic control with my teammate.
     3. I felt productive, because I think no time was wasted, I had a lot of fun from the game and I also got the job done.
     4. I felt successful, I was in a good mood when I went to bed.
  * 2018-12-26
     1. I had a discussion about the project at night, then I had another discussion about the professional skills, so tws discussion a day.
     2. after the discussion, I studied automatic control for half an hour, then I take a rest, played computer game.
     3. I felt little bit unproductive, because I was tired during day time, and sometimes I don't know what was I doing, time was wasted.
     4. I still felt successful.
     5. I shouldn't stay up late.
     
#Produtive and Successful
   * 2018-12-21
     1. I went to Taipei and help my brother for Taipei International Audio&Art Show.
     2. I feel productive, because I learned a lot about audio system engineering.
     3. I feel successful, because I help my brother a lot, and I was busy whole day.
   * 2018-12-22
     1. I went date with my girlfriend on Saturday.
     2. Productive, because I played around with my girlfriend whole day, I think no time was wasted, it was a good time.
     3. Successful, because I had a lot of fun.
   * 2018-12-23
     1. It was the last day of the exhibition, so I helped my brother all the day.
     2. I feel productive, because I learned a lot.
     3. I feed successful, because I can explain more detail about the product to the visitor.
   * 2018-12-24
     1. After finished quiz and expriment in the morning, I went home and played computer game.
     2. I feel little bit unproductive, because I think there was an hour I just doing nothing, not even playing game.
     3. I feel successful, it still was a great relax for me after the midterm and the exhibition.
     4. To be more productive, I should remind myself I was doing nothing when I drifted out.
  * 2018-12-25
     1. After the class, I had my lunch and went back to bed, slept for an hour, then I finished the task which my brother gave me,then I played computer game.
     2. At night, I had a discussion about the project of automatic control with my teammate.
     3. I felt productive, because I think no time was wasted, I had a lot of fun from the game and I also got the job done.
     4. I felt successful, I was in a good mood when I went to bed.
  * 2018-12-26
     1. I had a discussion about the project at night, then I had another discussion about the professional skills, so tws discussion a day.
     2. after the discussion, I studied automatic control for half an hour, then I take a rest, played computer game.
     3. I felt little bit unproductive, because I was tired during day time, and sometimes I don't know what was I doing, time was wasted.
     4. I still felt successful.
     5. I shouldn't stay up late.
     
#2018-12-27
  * Unitychain
     1. I think the concept is really cool, really excited about how much impact would they bring to the world.
  * Presentation
     1. I think the app solution is really great, but I think we don't have enough time to do that.
  * What make something go viral
     1. Make me laugh.
     2. Bring everybody together.
     3. This is me.
     
#Produtive and Successful
  * 2018-01-03
     1. After class, I went home and study for fluid mechanic final.
     2. I felt unsuccessful, because fluid mechacis is so difficult, and I forgot to eat dinner.
     3. I felt productive, because I finished the study.
 * 2018-01-04
     1. Finished the final in the morning, I went home and thought about the project of the Computer Graphic, and I slept for about an hour.
     2. I felt successful, because I finished one final, time to relax a bit and prepare the next final.
     3. I felt unproductive, because I still didn't what I am going to do for the project.
 * 2018-01-05
     1. In daytime, I studied Mechine Component Design final, and started to code for the CG project.
     2. I felt unsuccessful, because Mechine Component Design is hard, and little bit boring to me.
     3. I felt productive, because I knew what I gonna do for the project.
  * 2018-01-06
     1. Prepare the final.
     2. I felt successful, because I finished the studied.
     3. I felt unproductive, because Mechine design took me so much time, I did't code for project.
  * 2018-01-07
     1. After the final, I went home and coded for the project.
     2. I felt successful, because I think the final was not too difficult.
     3. I felt productive, because I finished the project and ppt for presentation.
  * 2018-01-08
     1. Prepared electronic and automatic control final, and discussed about the presentation of fluid mechanic.
     2. I felt unsuccessful, because the discussion took so many time, and I think I didn't have much contribution to the presentation.
     3. I felt unproductive, because I didn't have good process on the final.
  * 2018-01-09
     1. Prepared the final all day, and present for the project of CG.
     2. I felt successful, because the presentation went quite well.
     3. I felt productive, because I finished the studied.
  * 2018-01-10
     1. AUTOMATIC CONTROL BOOOOOM.
