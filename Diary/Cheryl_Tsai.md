## 2019-09-19

* For first, talking about the conflicts parts in the beginning of the class, I think we all know that collaboration is the most important part to let us get progress for our discussion and projects. Therefore, we should learn how to do this in a better way.
* I thought that I won't be able to understand how git this programs work; however, I was able to get to know it deeply for searching it on the Internet.
* At the beginning, I thought I won't be that unlucky to upload my document in problems. However, my first time using this is totally in a mess. I wrote a lot in my diary but it all disappeared which I have to write it again.
* I am extremely interested in the topic of "Disruptive Innovation and Exponentional growth". The reason why I have this kind of thought is because I have never been focusing on the products that cause innovation in our environment and found out that how this impact our life.
* I love the chart of showing impact of innovation of platforms on economics. That chart is so cool showing the quantities of the products that have been invented.
* The quote that Theodore said I think is inspiring. He could use a sentence that formal but easy to describe an information that we might know but couldn't say that precisely.
* Steven Pinker in the video has mentioned that although the war, poverty and pollution have been reduced but we should focus on problems like climate change and nuclear war instead of thinking about the waiting apocalypses.

## 2019-09-26

* In the video of 3 ways to spot a bad statistics, the presenter has mentioned that private company doesn't have the interest of getting the right numbers.Also, although government's statistic might be more accurate, we still have to double check by ourselves.
* The speaker called Hans Rosling, the way he debunks how this so-called developing world is going on was shocked and impressive. Since that we always believe in the stats that government or corporation show us. However, the stats might not be that accurate and we should double check instead of believing in it.
* I think that what Hans Rosling has addressed is worthwhile for us to think about it. We all know that, for example, people in this world want to help those who are still in un-development country.However, the stats always show the total gdp or economy state of Africa or Latin America but never seperate them into countries. Therefore, we should know how to see through every datas instead of just seeing the surface of the stats that have been shown.

## 2019-10-03
* For this week, we talked about how the financial system works in a country knowing that debt affects to the economy the most.
* Also known from the video that GDP doesn't related to quality but quantity.

## 2019-10-17

* In the video of Yuval Noah Harari, I think that this video is extremely impressive since that he has mentioned that we need to know what our weakness is so that it won't become the weapon. The reason why I love this sentence is because we all want the world that we are living now become a better place to stay.
* I've known the difference between facism and nationalism, for example, whether they care only themselves or everyone.
* For the second video that addressed by Christian, what I have learned is that don't believe in everything that people have been told. We should hear the voice in our hear and follow it. We might encounter in some difficulties; we should seek for the help from family, friends or organization that could help us and not just get through the wrong way.
* After Christian's speech, I think that people all deserve second chance to makeup their mistake. We are all human beings and we all do wrong things sometimes in our life. It's fair that we have second chance to live our life in a better way.

## 2019-10-24

* I think the topic "the path to knowledge" is interesting. The reason why I have this thought is because we can search whether there is evidence to prove our point or no. Also knowing the thought that we have is whether right or wrong.
* I like the discussion that we have with our classmates. During the discussion, we are able to know everyone' s opinions and thoughts and to compensate the knowledge that we don't have.
* In the video of the presenter Wendy, I love the way she addressed the speech and I do agree to her points that workout makes us healthier and better. Since that I hated to exercise before, but after start to exercise, I feel like I have more energy than before and I think I get better at remembering things.
* In the second video of the presenter Matthias Mllenbeck, I'm not saying that his speech is bad but I partially agree to his points. Because the point that he has I think it is not that practical and it is hard to achieve. People get sick most of the reason is because environment and their life style or even age; therefore, this is actually hard to prevent. However, the thought that he has might still work in the future!

## 2019-10-31

* We all know that we will sometimes feel depressed when we met difficulties in our life. It is somtimes easy to solve; however, somtimes might not be that easy as we wished. Therefore, people might get into the hole and couldn't survive which caused depression.
* Bill Bernat in the video has mentioned that don't be afriad to show consideration and love to those who need your help. If you are afriad that you don't know what to say, you can even just sit beside them and listen to them. Since that listening is vital for those who are in depression, just show some grace or even humor to make them feel better.
* The presentation from Kevin Briggs for me is extremely impressive. I rememebered that he mentioned that everyone desrves a second chance in thier life whcih makes me touched. Since that everyone is imperfect, it is hardly not to make mistake in our life. We deserve a chance to remedy it

## 2019-11-05

* For the main idea of today's class is about how to find hapiness in our life.
* The first picture that the professor showed in the beginning is really impressive. Since that I have never seen this kind of activity and this activity can let us think about our life deeply and try to focus on what we should actually do in our life to make it colorful and better!
* For the first presenter, I didnt really get what he is trying to convey but I think he is trying to tell us that we can use differnt ways to view our life instead of focusing a specific way.
* I really respect the second presenter of the video. If it for me, I wouldn't be able to convince myself to forgive the one who had killed my family. But after watching this video, I think that I should learn how to forgive others to make myself a better one.

## 2019-11-14

* Today's topic is about different law that we might meet in our daily life.
* I think this topic is really interesting. Since thar I have never thought about unstanding and searching all this topic by myself but today I got the chance to understand everything during the class!
* The most impressive sentence that has been mentioned in the video is that " the threat given by someone is the one should be arrested." I think that this is definitely accurate since that not everyone will do the right thing or right decision all the time. sometimes even a police might do the wrong thing. Therefore, we should think deeply when the worst thing happened and what should we do if we are the one involved in the situation.
* After today's topic, I think that I should search more information about this kind of topic to boarden my horrizons.

## 2019-11-21

* The topic of today is about how to stay connected and free. In the beginning, I thought that this topic would be extremely boring but it turned out actually really interesting. 
* From one of the video, we can understand that people nowadays are easily to be phone addicted. Therefore, they couldn't live without their phones but to stick to them. The thing that we do when we wake up probably is to check our phone no matter for social media or to check the messages.
* I think that we should really change this habit into maybe stretch our body or drink a cup pf warm water to refresh our digestic system. Since that being addicted to our phone isn't a good choice but to affect our life in the bad way.
* In addition, because the technology has been advanced and become better than before, people can easily get the information what they want on the internet. However, the information on the internet might be a fake one. Hence, we should apply the skills that we have learned in the previous class to distinguish whether that is real or fake informstion.

## 2019-11-29

* Today we are talking about how to make sense of events. First time listening to this topic I think that it's really abstract that I couldn't guess what this topic is about. However, I feel super intersted in this topic after the professor talked about the idea that we are going to discuss today.
* The most impressive sentence of today is that "Rules are needed to define where one persons freedom ends and another persons start."
* This sentence really means a lot to me. The reason is that I think nowadays we emphasize that human beings should all have their rights and freedom to choose what we want. However, we therefore often forget others' feeling but to focus on our own self.
* Therefore, we need rules to be set for telling us the range and letting us not only care about our own self but to also focus on others.

## 2019-12-05

* In today's class, we are talking about planetary boundaries.
* Before this lecture, I have already fallen in love with this topic and after this course, I have gained more knowledge about this topic which I am really happy about that.
* In the first video, we can learn that we have been wasting too many resouces on the earth that can't not even be reused which we get less and less resources to utilize.
* I really like the way the speaker convey his thought to us letting us know how to make the resources being reused and talked about the planetary boundaries to proect our own earth.

## 2019-12-12

* In today's class,we have a different kind of lecture today. Since that we used to watch the videos while the course. However, this time the professor lets the classmates to discuss the topic and the lecture which I think is actually really cool.
* I like the way that we have the course for this time. Because we can have more discussion, we learn more from the lecture.

## 2019-12-19

* I think that everyone prepared a lot of information for their presentation.
* I really like the lecture for today which I think that I have learned a lot today!

## 2019-12-26

* Doing the topic for today is actually the hardest task that I have ever done. Since that we have different opinions for the topic, it is hard for us to reach the agreement till the end.
* However, I think the presentation went out alright because we have prepared enough information.
* I am looking forward to the next week topic because we are combining the group together and do the presentation.

## 2020-01-02

* I think that the questions the professor asked the classmates were interesting. Because those questions sounded not that hard but they actually needed time to figure out.
* The most favorite part of today's course is the discussion that we have during the class for the next week final exams!

## 2020-01-09

* Today is the last day of this course which I acutally felt a bit sad since that I have known so many new people because of this class.
* We have exams and presentation today and I love the presentations so much!