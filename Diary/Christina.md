This diary file is written by Christina F94085321 in the course Professional skills for engineering the third industrial revolution.

# 2021-09-23 #

* Introduced to GIT Repository (a bit tricky to use, but love the neat and simple layout), wondering why GIT Repository instead of Google Drive.
* Glasl conflict escalation model is interesting.
* Give a presentation and received a lot of feedbacks from Prof (realised the importance of formatting details in making the slides look professional).
* I think exponential might not happen forever, eg. electronic components have physical size limitation, they can't be continually shrunk forever.
* Got new insights about the 5 major pillars of industrial revolution 4.0.
* The Covid-19 pandemic has put us into a setback in reaching SDGs.